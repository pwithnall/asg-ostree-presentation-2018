\usepackage{fontspec}
\usepackage{polyglossia}
\setdefaultlanguage{english}
\usepackage{hyperref}
\hypersetup{bookmarks=true, colorlinks=false, linkcolor=blue, citecolor=blue, filecolor=blue, urlcolor=blue,
            pdftitle=Peer to peer OS and flatpak updates,
            pdfauthor=Philip Withnall, pdfsubject=, pdfkeywords=}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,automata,positioning,er,calc,decorations.pathmorphing,fit}
\usepackage{listings}
\usepackage{subfigure} % can't use subfig because it doesn't support Beamer
\usepackage{fancyvrb}
\usepackage{ulem}
\usepackage{siunitx}
\usepackage[Latin,Greek,Emoticons]{ucharclasses}
\usefonttheme{professionalfonts} % using non standard fonts for beamer

% Syntax highlighting colours from the Tango palette: http://pbunge.crimson.ch/2008/12/tango-syntax-highlighting/
\definecolor{LightButter}{rgb}{0.98,0.91,0.31}
\definecolor{LightOrange}{rgb}{0.98,0.68,0.24}
\definecolor{LightChocolate}{rgb}{0.91,0.72,0.43}
\definecolor{LightChameleon}{rgb}{0.54,0.88,0.20}
\definecolor{LightSkyBlue}{rgb}{0.45,0.62,0.81}
\definecolor{LightPlum}{rgb}{0.68,0.50,0.66}
\definecolor{LightScarletRed}{rgb}{0.93,0.16,0.16}
\definecolor{Butter}{rgb}{0.93,0.86,0.25}
\definecolor{Orange}{rgb}{0.96,0.47,0.00}
\definecolor{Chocolate}{rgb}{0.75,0.49,0.07}
\definecolor{Chameleon}{rgb}{0.45,0.82,0.09}
\definecolor{SkyBlue}{rgb}{0.20,0.39,0.64}
\definecolor{Plum}{rgb}{0.46,0.31,0.48}
\definecolor{ScarletRed}{rgb}{0.80,0.00,0.00}
\definecolor{DarkButter}{rgb}{0.77,0.62,0.00}
\definecolor{DarkOrange}{rgb}{0.80,0.36,0.00}
\definecolor{DarkChocolate}{rgb}{0.56,0.35,0.01}
\definecolor{DarkChameleon}{rgb}{0.30,0.60,0.02}
\definecolor{DarkSkyBlue}{rgb}{0.12,0.29,0.53}
\definecolor{DarkPlum}{rgb}{0.36,0.21,0.40}
\definecolor{DarkScarletRed}{rgb}{0.64,0.00,0.00}
\definecolor{Aluminium1}{rgb}{0.93,0.93,0.92}
\definecolor{Aluminium2}{rgb}{0.82,0.84,0.81}
\definecolor{Aluminium3}{rgb}{0.73,0.74,0.71}
\definecolor{Aluminium4}{rgb}{0.53,0.54,0.52}
\definecolor{Aluminium5}{rgb}{0.33,0.34,0.32}
\definecolor{Aluminium6}{rgb}{0.18,0.20,0.21}

\AtBeginSection{\frame{\sectionpage}}

% Abstract here: https://cfp.all-systems-go.io/en/ASG2018/public/events/220
% Recently, work that we have been doing on Endless OS to allow peer to peer OS
% and flatpak updates has been reaching maturity and nearing wider deployment.
% This talk will give an overview of how we support LAN and USB updates of
% OSTrees, how it fits in upstream in OSTree and flatpak, and what you’d need
% to do to enable peer to peer updates for your OSTree system.
\title{Peer to peer OS and flatpak updates}

\author{Philip Withnall\\Endless Mobile\\\texttt{philip@tecnocode.co.uk}}
\date{September 29, 2018}

\begin{document}


\begin{frame}
\titlepage
\end{frame}

\note{25 minutes allocated. 15 minutes for talk, 10 minutes for questions.}


\begin{frame}{What are peer to peer OS and flatpak updates?}
\begin{itemize}
  \item{flatpak: Linux application sandboxing and distribution framework}
  \item{OS: in our case, Endless OS, based on OSTree}
  \item{OSTree: git for OS file system trees}
  \item{Peer to peer: getting updates from the LAN or a USB stick}
\end{itemize}
\end{frame}

\note{I’ll start with some basic terms just to get everyone on the same page. For those who already know what flatpak and OSTree are, I apologise; I’ll go into more
technical detail later in the talk.

flatpak is a Linux application sandboxing and distribution framework. For the purposes of this talk, all you need to know is that it’s based on OSTree, lots of apps
are packaged using it, and in particular Endless OS uses it for distributing and installing the majority of our desktop apps.

I work on Endless OS, which is an OSTree-based Linux distribution. We use OSTree to provide atomic updates and an immutable OS file system.

OSTree itself can be most basically described as git for OS file system trees. It’s a content-addressed object store, with a hard link farm used to deploy a bootable
OS. It has some glue for the bootloader to be able to boot one of several deployments.}

\begin{frame}{Some OSTree basics}
\begin{itemize}
  \item{An OSTree repository is a content addressed object store}
  \item{Refs, like git branches: named identifiers for commits, which can change over time}
  \item{Remotes, like git remotes, which are configured locally}
  \item{Each remote has a name, which is chosen locally}
  \item{flatpak is based on OSTree}
\end{itemize}
\end{frame}

\note{OSTree is an object store. An object is a commit, directory tree or a file, addressed by its checksum. Commits are a set of files. OSTree has refs, which are named pointers to
commits, which can be updated over time. Like git, OSTree has remotes, which are configured locally and say where you can update your refs from. Each ref can be local
or come from a remote (also like git).

Each flatpak app is a ref, and each version of a flatpak app is a commit. A single flatpak repository can contain lots of apps, each with its own ref. Naming convention
can make different versions of an app available, for example for different architectures, or stable/unstable versions.}

\begin{frame}{How did we add peer to peer support?}
\begin{itemize}[<+->]
  \item{What we need: a global namespace}
  \item{How do we add that? Collection IDs}
  \item{Collection IDs: like remotes, but configured globally rather than locally}
\end{itemize}
\end{frame}

\note{So if you want to do peer to peer updates, you need a way to find the latest commit for a given OSTree ref. This works the same for flatpak apps as it does for
an OS: both are just an OSTree commit on a ref. You want the most up to date commit.

How do you find the most up to date commit for a given ref? Look it up. However, refs are not in a global namespace, so you can’t. \texttt{os/MyOs} on one repository
might not be semantically the same as \texttt{os/MyOs} on another repository.

So the first step in doing peer to peer updates was to add a global namespace for refs. We did this by adding collection IDs, which are basically a globally unique
version of the names used for remotes. The key difference is that they’re set by the remote repository, and advertised by it; rather than being set in a user’s
local configuration and only kept the same through convention.

For example, the collection ID used by the main flathub repository is \texttt{org.flathub.Stable}.}

\begin{frame}{A problem: summary files}
\begin{itemize}[<+->]
  \item{OSTree summary file traditionally signed as a blob}
  \item{Lists refs in an OSTree repository}
  \item{What if I want to advertise my computer’s repository on the LAN?}
  \item{Can’t glue together multiple upstream summary files and re-sign them}
\end{itemize}
\end{frame}

\note{There was one other problem with introducing peer to peer updates in OSTree: its \texttt{summary} file. This was a file at a well-known location in a repository
which contained (amongst a few other things) a cache of the mapping from refs to commits. The canonical mapping is kept on the file system, as one file per ref, at
a path \texttt{\$repo/refs/heads/\$ref}, with each ref file containing the checksum of the commit it currently points to. But a cache was needed for efficiency.
To avoid attackers making refs point to the wrong commits, the \texttt{summary} file is signed by the repository owner, using their key.

The problem with this is that if you have a repository which you want to expose on the LAN, containing some refs from one remote, and some refs from another, which
\texttt{summary} file do you publish? You need to publish a hybrid of two, but then you can’t publish a valid signature for it, with any key.}

\begin{frame}{Introducing ref bindings}
\begin{itemize}[<+->]
  \item{Solution: drop summary file signatures}
  \item{Bind commits to refs rather than refs to commits}
  \item{Store ref name in each commit’s (signed) metadata}
\end{itemize}
\end{frame}

\note{The solution was to split the signature up, and invert the binding. Instead of cryptographically binding each ref to a commit; cryptographically bind all commits
to zero or more refs. Each commit was signed by the repository already, so just add a ref binding to its metadata. The \texttt{summary} file remains as an unsigned cache,
and any ref mappings taken from it must have their reverse ref bindings checked before they can be trusted.

This approach necessitates some changes in how repositories are managed (commits must be regenerated to update a ref), but it’s not been a problem.}

\begin{frame}{Now the groundwork is in place…}
\begin{itemize}
  \item{USB updates: put an OSTree repository on a USB stick (in \texttt{.ostree})}
  \item{LAN updates: expose an OSTree repository with a local web server}
\end{itemize}
\end{frame}

\note{With the groundwork in place, an OSTree ref can be pulled from anywhere, as long as you have its name, collection ID, and the GPG key to validate it with. To put
OSTree refs on a USB stick, simply put an OSTree repository in a well-known place on the USB stick. To put OSTree refs on the LAN, run a local web server and advertise
it on the network.}

\begin{frame}{How do you efficiently find refs on a busy LAN?}
\begin{itemize}[<+->]
  \item{With a LAN of 30 machines, each with 100 refs, many at different versions, how do you quickly find which machines have refs you want?}
  \item{Put a bloom filter of refs in a DNS-SD record}
  \item{Download the summary file from each matching peer and check in detail}
\end{itemize}
\end{frame}

\note{How do you do that efficiently? Not every peer will have every ref, or have it up to date. And how do you do this in a privacy-preserving way?

Each peer publishes a DNS-SD record containing a bloom filter of the refs they have available. They also make their local OSTree repository, including its
\texttt{summary} file, available via a local web server. Any peer wanting to query another for updates will check if the ref they want is potentially in the other’s
bloom filter. If so, they will download the \texttt{summary} file from the other peer, and check to see if the match was a true positive, and whether the other peer
has a more recent commit for that ref. If so, they download the commit metadata and objects from the other peer.

This is privacy preserving because the only information that is broadcast to the entire network by each machine is a bloom filter. The only information which is
broadcast in a query is “do you have a bloom filter”. All other communication is unicast, and peers can decide whether to trust each other before replying in unicast
communications.}

\note{In future, this approach will potentially allow for parallel downloads from multiple local peers. That isn’t currently implemented. Work also needs to be done to
implement retries, backoffs, and some amount of randomisation in the choice of peer when there are multiple options.}

\begin{frame}{Where is all the code for this?}
\begin{itemize}
  \item{In upstream libostree and flatpak}
  \item{And in our updater: eos-updater}
  \item{Already supported by flathub!}
\end{itemize}
\end{frame}

\note{What of this is upstream, and what is specific to Endless OS? For USB updates, what’s in flatpak and libostree upstream is all you need. There are tools to create
USB update sticks, and to find them when searching for updates.

The flathub repository of apps already has a collection ID, so all its refs are ready to be used peer to peer.}

\begin{frame}{What does eos-updater do?}
\begin{itemize}
  \item{Contains the web server and DNS-SD record generator for sharing LAN updates}
  \item{DNS-SD record generator provides configuration for Avahi}
\end{itemize}
\end{frame}

\note{For LAN updates, that code also exists in flatpak and libostree; but you need another couple of components: one to create the DNS-SD records, and one to be the local
web server for an OSTree repository. Since was no suitable daemon for this in OSTree already, we put the code in eos-updater (our OS update daemon), but it’s free
software.}

\begin{frame}{Who worked on it?}
\begin{itemize}
  \item{Endless: Matthew Leeds, Rob McQueen, Dan Nicholson, Krzesimir Nowak, me}
  \item{OSTree developers: Colin Walters, rh-atomic-bot {\setsansfont{Noto Emoji}😍}}
  \item{flatpak developers: Alex Larsson}
\end{itemize}
\end{frame}

\note{While I have the pleasure of giving the talk today, I’ve only done some of the work. It’s been worked on and reviewed by a lot of people within and outside
of Endless. Thanks!}

\begin{frame}{Miscellany}
\begin{description}
  \item[OSTree]{\url{https://github.com/ostreedev/ostree/}}
  \item[flatpak]{\url{https://github.com/flatpak/flatpak}}
  \item[eos-updater]{\url{https://github.com/endlessm/eos-updater}}
\end{description}

\vfill
\begin{center}
{\setsansfont{Noto Emoji}🐦} @pwithnall
\end{center}

\vfill
\begin{center}
Endless are hiring! \url{https://jobs.lever.co/endless}
\end{center}

% Creative commons logos from http://blog.hartwork.org/?p=52
\vfill
\begin{center}
	\includegraphics[scale=0.83]{cc_by_30.pdf}\hspace*{0.95ex}\includegraphics[scale=0.83]{cc_sa_30.pdf}\\[1.5ex]
	{\tiny\textit{Creative Commons Attribution-ShareAlike 4.0 International License}}\\[1.5ex]
	\vspace*{-2.5ex}
\end{center}
\end{frame}

\note{I should point out that Endless are currently hiring for a desktop engineer and an infrastructure/tools engineer. Talk to me or visit our website.}

\end{document}
